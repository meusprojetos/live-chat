# Live chat

Example of a real-time chat using  [style guide] @john_papa


## Tech:
* [AngularJS] - HTML enhanced for web apps!
* [TDD] -(unit testing);
* [Passport] Simple, unobtrusive authentication for Node.js
* [node.js] - evented I/O for the backend
* [Express] - Fast node.js network app framework
* [Gulp] -  (automation) the streaming build system
* [MongoDB] - (database);
* [Mongoose] - Mongoose is a MongoDB object modeling tool designed to work in an asynchronous environment.
* [Heroku] -  http://tableless.com.br/como-publicar-aplicacao-nodejs-heroku

## Install

```sh
$ git clone https://gitlab.com/meusprojetos/live-chat.git live-chat
$ cd live-chat
$ npm instal
```


[style guide]: <https://github.com/johnpapa/angular-styleguide/blob/master/i18n/pt-BR.md>
[node.js]: <http://nodejs.org>
[Passport]: <http://passportjs.org/>
[@jonh_papa]: <http://twitter.com/jonh_papa>
[express]: <http://expressjs.com>
[AngularJS]: <http://angularjs.org>
[Gulp]: <http://gulpjs.com>
[Heroku]: <https://www.heroku.com>
[mongoDB]:<http://mongodb.org>
[mongoose]: <https://www.npmjs.com/package/mongoose>
[tdd]: <http://tableless.com.br/tdd-por-que-usar/>

### Contributing
- Fork it!
-Create your feature branch: `git checkout -b my-new-feature`
- Commit your changes: `git commit -m 'Add some feature'`
- Push to the branch: `git push origin my-new-feature`
- Submit a pull request.

##### License
### MIT